# Interfacing the logical world / Plugin
## Übersicht
Zusätzlich zum «digitalen Bild» können Daten von externen Diensten und Programmen einbezogen werden, die bereits digital zur Verfügung stehen. So kann auch auf Wettervorhersagen statt nur die aktuellen Messwerte oder auf den zukünftigen Aufenthaltsort gemäss Kalender statt nur auf die aktuell per GPS ermittelte Position reagiert werden.
Ein selbstfahrendes Auto könnte also beispielsweise selber auf eine Hagelwarnung reagieren und an einen geschützten Platz fahren.

### Mögliche Datenquellen (unter anderem)
* PIM (Personal Information Manager)
	* Kalender
	* Tasks
	* Kontakte
* e-Mail
* Social Media
* Wetter-Prognose
* Börsen-Daten
* Fahrplan
	* Verspätungen
* Verkehrsdaten
	* Staumeldungen
* Web-Content allgemein (scraping)

## REST zu MQTT
Die meisten Dienste bieten nur eine REST-API an, wir wollen aber live Daten. Deshalb muss die REST-API in regelmässigen Abständen nach neuen Resultaten abgefragt werden. Eine möglichst häufige Abfrage bringt uns zwar näher an real-time-Daten, wir riskieren aber, dass unsere Anfragen wegen des Rate-Limits abgelehnt werden.

Bei Twitter dürfen z.B. pro Applikation und 15min-Zeitfenster höchstens 900 Suchanfragen gestellt werden. Dies gilt pro Applikations-Key, also auch, wenn unsere Applikation an verschiedenen Standorten läuft und überall ein anderer User eingeloggt ist. Einige Endpoints haben aber sogar ein Limit von nur 15 Anfragen pro 15 Minuten.
[Twitter Rate Limits](https://developer.twitter.com/en/docs/basics/rate-limits.html)

## Anbindung Kalender
Um einen Kalender anzubinden gibt es verschiedene APIs. Je nachdem welchen Kalender man anbinden möchte, gibt es eine andere API. Hier ein paar Beispiele:
* [Outlook Calendar REST API] (https://docs.microsoft.com/en-us/previous-versions/office/office-365-api/api/version-2.0/calendar-rest-operations) (Outdated)
* [Microsoft Graph REST API] (https://docs.microsoft.com/en-us/graph/overview)
* [Google Calendar API] (https://developers.google.com/calendar/overview)
* [Android Calendar Provider API] (https://developer.android.com/guide/topics/providers/calendar-provider)
* [IoT Calendar] (https://icalendar.org/iot.html)
* [Apple Eventkit] (https://developer.apple.com/documentation/eventkit)

## Anbindung Social Media am Beispiel Twitter
- [twitter4j](http://twitter4j.org/en/code-examples.html): Ermöglicht den Zugriff auf Twitter mit sehr wenig Code (5 Zeilen für Suchabfrage + Ausgabe der Tweets)

```java 
    @Override
    public void run() {
        try {
            // ab wann wollen wir die neuen Tweets?
            // ID des neusten Tweets aus der vorherigen Abfrage
            query.setSinceId(latestTweetRecieved);
            
            QueryResult queryResult = twitter.search(query);
            List<Status> tweets = queryResult.getTweets();
            for(TwitterResultConsumer consumer : consumers) {
                consumer.receiveResult(tweets);
            }

            if(tweets.size() > 0) {
                // ID des neusten Tweets merken,
                // um bei der nächsten Anfrage als Grenze zu verwenden
                latestTweetRecieved = tweets.get(0).getId();
            }
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    public void execute() {
        Timer timer = new Timer();
        // run() alle searchInterval Sekunden ausführen
        timer.schedule(this, 0L, searchInterval* 1000L);
    }
```

Der Tweet-Watcher im Ordner TwitterExample führt eine Suche periodisch durch und leitet das Resultt via MQTT weiter.
Aufruf:
```java
MqttClient mqttClient = new MqttClient("tcp://localhost:1883", "TwitterMqttForwarder");
TwitterMqttForwarder twitterMqttForwarder = new TwitterMqttForwarder("twitterexample/hashIOT", mqttClient);
TweetWatcher tweetWatcher = new TweetWatcher("#IoT");
tweetWatcher.addConsumer(twitterMqttForwarder);
tweetWatcher.execute();
```

### Donts
- Immer gern gesehen: API-Keys auf GitHub: https://github.com/search?q=delete+twitter4j.properties&type=Commits
