package ch.adrianaulbach.mc3.tweetwatcher;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import twitter4j.JSONObject;
import twitter4j.Status;

import java.util.List;

public class TwitterMqttForwarder implements TwitterResultConsumer {
    private final String topic;
    private final MqttClient mqttClient;

    public TwitterMqttForwarder(String topic, MqttClient mqttClient) {
        this.topic = topic;
        this.mqttClient = mqttClient;
    }

    @Override
    public void receiveResult(List<Status> tweets) {
        try {
            if(!mqttClient.isConnected()) {
                mqttClient.connect();
            }

            for(Status tweet : tweets) {
                mqttClient.publish(topic, new MqttMessage(tweetToJsonString(tweet).getBytes()));
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    private String tweetToJsonString(Status tweet) {
        JSONObject json = new JSONObject();

        JSONObject userJson = new JSONObject();
        userJson.put("name", tweet.getUser().getName());
        userJson.put("screenname", tweet.getUser().getScreenName());

        json.put("contributors", tweet.getContributors());
        json.put("time", tweet.getCreatedAt());
        json.put("author", userJson);
        json.put("content", tweet.getText());

        return json.toString();
    }
}
