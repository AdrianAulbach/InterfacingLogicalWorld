package ch.adrianaulbach.mc3.tweetwatcher;

import twitter4j.*;

import java.util.*;

public class TweetWatcher extends TimerTask {
    private final String queryString;
    private Query.ResultType resultType = Query.ResultType.recent;
    private Twitter twitter = TwitterFactory.getSingleton();
    private int searchInterval = 20;
    long latestTweetRecieved = 0;

    private Query query;
    private Set<TwitterResultConsumer> consumers;

    public TweetWatcher(String queryString) {
        this.queryString = queryString;
        this.query = new Query(queryString);
        this.consumers = new HashSet<>();
    }

    public void setResultType(Query.ResultType resultType) {
        this.resultType = resultType;
    }

    public Query.ResultType getResultType() {
        return resultType;
    }

    public void addConsumer(TwitterResultConsumer consumer) {
        consumers.add(consumer);
    }
    public void removeConsumer(TwitterResultConsumer consumer) {
        consumers.remove(consumer);
    }

    @Override
    public void run() {
        try {
            query.setSinceId(latestTweetRecieved);
            QueryResult queryResult = twitter.search(query);
            List<Status> tweets = queryResult.getTweets();
            for(TwitterResultConsumer consumer : consumers) {
                consumer.receiveResult(tweets);
            }

            if(tweets.size() > 0) {
                latestTweetRecieved = tweets.get(0).getId();
            }
        } catch (TwitterException e) {
            e.printStackTrace();
        }
    }

    public void execute() {
        Timer timer = new Timer();
        timer.schedule(this, 0L, searchInterval* 1000L);
    }
}
