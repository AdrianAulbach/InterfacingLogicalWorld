package ch.adrianaulbach.mc3.tweetwatcher;

import twitter4j.Status;

import java.util.List;

public interface TwitterResultConsumer {
    void receiveResult(List<Status> tweets);
}
