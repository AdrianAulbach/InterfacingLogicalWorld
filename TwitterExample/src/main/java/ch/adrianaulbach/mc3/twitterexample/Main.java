package ch.adrianaulbach.mc3.twitterexample;

import ch.adrianaulbach.mc3.tweetwatcher.TweetWatcher;
import ch.adrianaulbach.mc3.tweetwatcher.TwitterMqttForwarder;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class Main {
    public static void main(String[] args) {
        String queryString = "#IoT";
        try {
            TwitterMqttForwarder twitterMqttForwarder = new TwitterMqttForwarder("twitterexample/" + "hashIoT", new MqttClient("tcp://localhost:1883", "TwitterMqttForwarder"));
            TweetWatcher tweetWatcher = new TweetWatcher(queryString);
            tweetWatcher.addConsumer(twitterMqttForwarder);

            tweetWatcher.execute();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }
}
